# This script aim to set PSDK in a 32x32 resolution instead of the 16x16 one
# The screen will then be in 640x480 and the viewports will be in 640x480 (you can choose the resolution)

# Fix the shadow & the screen x/y
class Game_Character
  # Return the x position of the shadow of the character on the screen
  # @return [Integer]
  def shadow_screen_x
    return (@real_x - $game_map.display_x + 3) / 4 + 16 # +3 => +5
  end

  # Return the y position of the shadow of the character on the screen
  # @return [Integer]
  def shadow_screen_y
    return (@real_y - $game_map.display_y + 3) / 4 + 32 + (@offset_shadow_screen_y || 0) + (@slope_offset_y || 0) / 2 # +3 => +5
  end

  # Return the x position of the sprite on the screen
  # @return [Integer]
  def screen_x
    return (@real_x - $game_map.display_x + 3) / 4 + 16
  end

  # Return the y position of the sprite on the screen
  # @return [Integer]
  def screen_y
    y = (@real_y - $game_map.display_y + 3) / 4 + 32 # +3 => +5
    y += @offset_screen_y if @offset_screen_y
    y += @slope_offset_y if @slope_offset_y
    if @jump_count >= @jump_peak
      n = @jump_count - @jump_peak
    else
      n = @jump_peak - @jump_count
    end
    return y - (@jump_peak * @jump_peak - n * n) / 2
  end
end

# Fix the particles
module Yuki
  class Particle_Object
    # Update the position of the particle sprite
    def update_sprite_position
      case @position_type
      when :center_pos, :grass_pos
        @sprite.x = ((@x * 128 - $game_map.display_x + 3) / 4 + 16)
        @sprite.y = ((@y * 128 - $game_map.display_y + 3) / 4 + 32)
        if @position_type == :center_pos || @sprite.y >= @character.screen_y
          @sprite.z = (screen_z + @add_z)
        else
          @sprite.z = (screen_z - 1)
        end
        @sprite.ox = @ox + @ox_off
        @sprite.oy = @oy + @oy_off
      when :character_pos
        @sprite.x = @character.screen_x / @zoom
        @sprite.y = @character.screen_y / @zoom
        @sprite.z = (@character.screen_z(0) + @add_z)
        @sprite.ox = @ox + @ox_off
        @sprite.oy = @oy + @oy_off
      end
    end

    add_handler(:rect) do |data|
      @sprite.src_rect.set(*data)
      @ox = data[2] / 2
      @oy = data[3]
    end

    add_handler(:oy_offset) { |data| @oy_off = data&.*(2) + @params.fetch(:ox_offset, 0) }
    add_handler(:ox_offset) { |data| @ox_off = data&.*(2) + @params.fetch(:ox_offset, 0) }
  end
end
